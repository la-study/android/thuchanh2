package com.example.th2.model;

import java.io.Serializable;

public class Job implements Serializable {
    private int id;
    private String name, description, deadline;
    private int status;
    private boolean isCooperative;

    public Job(int id, String name, String description, String deadline, int status, boolean isCooperative) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.status = status;
        this.isCooperative = isCooperative;
    }

    public Job(String name, String description, String deadline, int status, boolean isCooperative) {
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.status = status;
        this.isCooperative = isCooperative;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isCooperative() {
        return isCooperative;
    }

    public void setCooperative(boolean cooperative) {
        isCooperative = cooperative;
    }
}
