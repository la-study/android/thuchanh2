package com.example.th2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.th2.model.Job;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    private final static String DATABASE_NAME = "th2.db";
    private final static int DATABASE_VERSION = 1;

    public Database(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "create table jobs(id integer primary key autoincrement, name text, description text, deadline date, status integer, isCooperative text)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createJob(Job job) {
        String query = "INSERT INTO jobs(name, description, deadline, status, isCooperative) VALUES(?, ?, ?, ?, ?)";
        String[] args = {job.getName(), job.getDescription(), job.getDeadline(), String.valueOf(job.getStatus()), String.valueOf(job.isCooperative())};

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(query, args);
    }

    public List<Job> getJobs() {
        List<Job> jobList = new ArrayList<>();
        String query = "SELECT * from jobs";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String description = cursor.getString(2);
            String deadline = cursor.getString(3);
            int status = cursor.getInt(4);
            boolean isCooperative = cursor.getString(5).equalsIgnoreCase("true");

            Job job = new Job(id, name, description, deadline, status, isCooperative);
            jobList.add(job);
        }
        cursor.close();
        return jobList;
    }

    public int updateJob(Job job) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", job.getName());
        contentValues.put("description", job.getDescription());
        contentValues.put("deadline", job.getDeadline());
        contentValues.put("status", job.getStatus());
        contentValues.put("isCooperative", job.isCooperative());

        String where = "id=?";
        String[] agrs = {Integer.toString(job.getId())};

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.update("jobs", contentValues, where, agrs);
    }

    public int deleteJob(int id) {
        String where = "id=?";
        String[] agrs = {Integer.toString(id)};
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.delete("jobs", where, agrs);
    }


    public List<Job> search(String key, int status) {
        List<Job> jobList = new ArrayList<>();
        String query = "SELECT * from jobs where (name like ? or description like ?)" + ((status != 0) ? " and status = ?" : "") + " order by deadline";

        System.out.println("search: " + key + " --- " + (status == 0) + query);
        String[] agrs = status == 0 ? new String[]{"%" + key + "%", "%" + key + "%"} : new String[]{"%" + key + "%", "%" + key + "%", String.valueOf(status - 1)};

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(query, agrs);

        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String description = cursor.getString(2);
            String deadline = cursor.getString(3);
            int _status = cursor.getInt(4);
            boolean isCooperative = cursor.getString(5).equalsIgnoreCase("true");

            Job job = new Job(id, name, description, deadline, _status, isCooperative);
            jobList.add(job);
        }
        cursor.close();
        return jobList;
    }
}
