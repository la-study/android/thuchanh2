package com.example.th2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.th2.R;
import com.example.th2.model.Job;

import java.util.List;

public class JobAdapter extends ArrayAdapter {
    Context context;
    List<Job> jobs;

    private JobItemListener jobItemListener;

    public JobAdapter(@NonNull Context context, List<Job> jobs) {
        super(context, R.layout.item, jobs);
        this.context = context;
        this.jobs = jobs;
    }

    public void setJobItemListener(JobItemListener jobItemListener) {
        this.jobItemListener = jobItemListener;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.item, null, true);
        TextView txtTen = v.findViewById(R.id.textViewTen);
        TextView txtDescription = v.findViewById(R.id.textViewDescription);
        TextView txtDate = v.findViewById(R.id.textDate);
        TextView txtCongTac = v.findViewById(R.id.textViewCongTac);
        TextView txtTinhTrang = v.findViewById(R.id.textViewTinhTrang);

        Job job = jobs.get(position);
        txtTen.setText(job.getName());
        txtDescription.setText(job.getDescription());
        txtDate.setText("Deadline: " + job.getDeadline());
        switch (job.getStatus()) {
            case 0:
                txtTinhTrang.setText("Chưa thực hiện");
                break;
            case 1:
                txtTinhTrang.setText("Đang thực hiện");
                break;
            case 2:
                txtTinhTrang.setText("Hoàn thành");
                break;
        }

        if (job.isCooperative()) {
            txtCongTac.setText("Làm chung");
        } else {
            txtCongTac.setText("1 mình");
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jobItemListener.onItemClicked(v, position);
            }
        });
        return v;
    }

    public interface JobItemListener {
        void onItemClicked(View v, int position);
    }
}
