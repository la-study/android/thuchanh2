package com.example.th2.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.th2.fragment.FilterJobFragment;
import com.example.th2.fragment.InfoFragment;
import com.example.th2.fragment.ListJobFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private int pageNumber;

    public FragmentAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.pageNumber = behavior;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListJobFragment();

            case 1:
                return new InfoFragment();

            case 2:
                return new FilterJobFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return pageNumber;
    }
}
