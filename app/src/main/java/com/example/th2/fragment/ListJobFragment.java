package com.example.th2.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.th2.FormActivity;
import com.example.th2.R;
import com.example.th2.adapter.JobAdapter;
import com.example.th2.database.Database;
import com.example.th2.model.Job;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ListJobFragment extends Fragment implements JobAdapter.JobItemListener {
    ListView listView;
    JobAdapter jobAdapter;
    List<Job> jobs;

    Database database;

    FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_job_fragment, container, false);
        floatingActionButton = v.findViewById(R.id.floatingActionButton);
        listView = v.findViewById(R.id.listView);
        database = new Database(getContext());
        jobs = new ArrayList<>();
//        jobs.addAll(database.getJobs());

        jobAdapter = new JobAdapter(getContext(), jobs);
        jobAdapter.setJobItemListener(this);
        listView.setAdapter(jobAdapter);


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FormActivity.class);
                startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onItemClicked(View v, int position) {
        Intent intent = new Intent(getContext(), FormActivity.class);
        Job job = jobs.get(position);
        intent.putExtra("selectedJob", job);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        jobs.clear();
        jobs.addAll(database.getJobs());
        jobAdapter.notifyDataSetChanged();
    }
}
