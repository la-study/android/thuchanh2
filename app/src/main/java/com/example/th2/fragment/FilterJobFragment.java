package com.example.th2.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.th2.R;
import com.example.th2.adapter.JobAdapter;
import com.example.th2.database.Database;
import com.example.th2.model.Job;

import java.util.ArrayList;
import java.util.List;

public class FilterJobFragment extends Fragment implements JobAdapter.JobItemListener {
    SearchView searchView;
    Spinner statusSpriner;
    Button searchBtn;
    ListView listView;
    List<Job> jobs;

    TextView totalText;

    Database database;
    JobAdapter jobAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_job_fragment, container, false);

        searchView = v.findViewById(R.id.searchView);
        statusSpriner = v.findViewById(R.id.statusSpinner);
        searchBtn = v.findViewById(R.id.searchBtn);
        listView= v.findViewById(R.id.listView);
        totalText = v.findViewById(R.id.totalText);

        totalText.setVisibility(View.GONE);

        jobs = new ArrayList<>();
        database = new Database(getContext());
        jobAdapter = new JobAdapter(getContext(), jobs);
        jobAdapter.setJobItemListener(this);
        listView.setAdapter(jobAdapter);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key = String.valueOf(searchView.getQuery());
                int status = (int) statusSpriner.getSelectedItemId();
                jobs.clear();
                jobs.addAll(database.search(key, status));
                jobAdapter.notifyDataSetChanged();

                totalText.setVisibility(View.VISIBLE);
                totalText.setText("Số kết quả: " + jobs.size());
            }
        });

        return v;
    }

    @Override
    public void onItemClicked(View v, int position) {

    }

    @Override
    public void onResume() {
        super.onResume();
        searchView.setQuery("", false);
        statusSpriner.setSelection(0);
        totalText.setVisibility(View.GONE);

        jobs.clear();
        jobAdapter.notifyDataSetChanged();
    }
}
