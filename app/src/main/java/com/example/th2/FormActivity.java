package com.example.th2;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.th2.database.Database;
import com.example.th2.model.Job;

import java.util.Calendar;

public class FormActivity extends AppCompatActivity {
    Button deleteBtn, submitBtn, cancelBtn, dateBtn;
    EditText textName, textDescription;
    TextView textDate;

    RadioGroup statusGroup;
    LinearLayout statusWrap;
    CheckBox isCooperativeCheckBox;

    Job selectedJob;

    Database database = new Database(this);

    @SuppressLint("MissingInflatedId")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        deleteBtn = findViewById(R.id.deleteBtn);
        submitBtn = findViewById(R.id.submitBtn);
        cancelBtn = findViewById(R.id.cancelBtn);
        dateBtn = findViewById(R.id.dateBtn);

        textName = findViewById(R.id.editTextName);
        textDescription = findViewById(R.id.editTextDescription);
        textDate = findViewById(R.id.textDate);
        statusWrap = findViewById(R.id.statusWrap);
        statusGroup = findViewById(R.id.statusGroup);
        isCooperativeCheckBox = findViewById(R.id.checkBox);

        Intent intent = getIntent();
        selectedJob = (Job) intent.getSerializableExtra("selectedJob");

        if (selectedJob != null) {
            submitBtn.setText("Cập nhật");
            textName.setText(selectedJob.getName());
            textDescription.setText(selectedJob.getDescription());
            textDate.setText(selectedJob.getDeadline());
            switch (selectedJob.getStatus()) {
                case 0:
                    statusGroup.check(R.id.todoRadio);
                    break;
                case 1:
                    statusGroup.check(R.id.onGoingRadio);
                    break;
                case 2:
                    statusGroup.check(R.id.doneRadio);
                    break;
            }
        } else {
            deleteBtn.setVisibility(View.GONE);
            submitBtn.setText("Thêm");
            statusWrap.setVisibility(View.GONE);
        }

        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int cy = c.get(Calendar.YEAR);
                int cm = c.get(Calendar.MONTH);
                int cd = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(FormActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textDate.setText(i + "/" + i1 + "/" + i2);
                    }
                }, cy, cm, cd);

                dialog.show();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                database.deleteJob(selectedJob.getId());
                finish();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = textName.getText().toString();
                String description = textDescription.getText().toString();
                String date = textDate.getText().toString();
                int status = 0;
                boolean isCooperative = isCooperativeCheckBox.isChecked();
                if (selectedJob != null) {
                    switch (statusGroup.getCheckedRadioButtonId()) {
                        case R.id.todoRadio:
                            status = 0;
                            break;
                        case R.id.onGoingRadio:
                            status = 1;
                            break;
                        case R.id.doneRadio:
                            status = 2;
                            break;
                    }
                }

                if (name.isEmpty() || description.isEmpty() || date.isEmpty()) {
                    Toast.makeText(FormActivity.this, "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_LONG).show();
                } else {


                    if (selectedJob != null) {
                        Job job = new Job(selectedJob.getId(), name, description, date, status, isCooperative);
                        database.updateJob(job);
                    }
                    else {
                        Job job = new Job(name, description, date, status, isCooperative);
                        database.createJob(job);
                    }
                    finish();
                }
            }
        });
    }
}
